import BusEvent from './bus';

Echo.join('chat-channel')
    .listen('ChatSentEvent', (e) => {
        BusEvent.$emit('chat.sent', e.data)
    });