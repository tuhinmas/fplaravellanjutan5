@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header"><h3>Transaction #{{$transaction->id}}</h3></div>
        <div class="card-body">
            <table class="table">
                <tr><td>customer id</td><td>:</td><td>{{$transaction->user->id}}</td></tr>
                <tr><td>customer name</td><td>:</td><td>{{$transaction->user->name}}</td></tr>
                <tr><td>total price</td><td>:</td><td>{{$transaction->total_price}}</td></tr>
                <tr><td>date</td><td>:</td><td>{{$transaction->created_at}}</td></tr>
            </table>
        </div>
        <div class="card-footer">
            <h6 class="btn btn-primary">Details</h6>
            <table class="table table-bordered table-striped">
                <tr><th>id</th><th>Name</th><th>Quantity</th><th>Price</th><th>SubTotal</th></tr>
                @foreach ($transaction->transaction_details as $detail)
                <tr>
                    <td>{{$detail->menu->id}}</td>
                    <td>{{$detail->menu->menu_name}}</td>
                    <td>{{$detail->quantity}}</td>
                    <td>{{$detail->menu->price}}</td>
                    <td>{{$detail->menu->price * $detail->quantity}}</td>
                </tr>                
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection
