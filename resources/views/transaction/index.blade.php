@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <table class="table table-bordered table-striped">
            <tr><th>Customer</th><th>Total Price</th><th>Date</th><th>Action</th></tr>
            @foreach ($transactions as $transaction)
            <tr>
                <td>{{$transaction->user->name}}</td>
                <td>{{$transaction->total_price}}</td>
                <td>{{$transaction->created_at}}</td>
                <td><a href="/transaction/{{$transaction->id}}" class="btn btn-primary">Detail</a></td>
            </tr>                
            @endforeach
        </table>
    </div>
</div>
@endsection
