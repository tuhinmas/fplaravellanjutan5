@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <menu-box-component />
        </div>
        <div class="col-md-5">
            <menu-cart-component/>
        </div>
    </div>
</div>
@endsection
