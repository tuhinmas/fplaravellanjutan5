<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Menu;
use Faker\Generator as Faker;

$factory->define(Menu::class, function (Faker $faker) {
    return [
        'menu_name' => $faker->name,
        'description' => $faker->realText(100),
        'price' => $faker->randomNumber(5),
    ];
});
